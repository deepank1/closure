
function limitFunctionCallCount(cb, n) {

    let count = 0;
    
    return (...args)=>{
        if(count<n){
            count = count + 1;
            return cb(...args)
        }
        else{
            return null;
        }
    }

}

module.exports = limitFunctionCallCount;