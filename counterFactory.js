

function counterFactory() {
    
    let object = {
        increment : (counter)=>{
            return counter + 1;
        },
        decrement : (counter)=>{
            return counter - 1;
        }
    }

    return object;
}

module.exports = counterFactory;