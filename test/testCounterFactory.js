let counter = 20;

let counterFactory = require("../counterFactory.js");

let inc = counterFactory().increment(counter);

let dec = counterFactory().decrement(counter)

console.log(inc);
console.log(counter);
console.log(dec);
console.log(counter);