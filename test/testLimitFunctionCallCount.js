function cb (val, val2){
    console.log(val * val2);
}

let limitFunctionCallCount = require("../limitFunctionCallCount.js");

let result = limitFunctionCallCount(cb, 3);


result(5,6);
result(4,5);
result(3,4);
result(2,3);
