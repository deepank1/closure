function cb(val, val2) {
    return val + val2;
}

let cacheFunction = require("../cacheFunction.js");

let result = cacheFunction(cb);

console.log(result(5, 3));
console.log(result(5, 3));
console.log(result(6, 3));
console.log(result(2, 3));