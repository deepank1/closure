

function cacheFunction(cb) {
    
    let cache = {};

    return (...arguments)=> {
        if(arguments in cache){
            console.log("cache full")
            return cache[arguments]
        }
        else{
            cache[arguments] = cb(...arguments);
            console.log("cache pushed");
            return cache[arguments];  
        }
    }
}


module.exports = cacheFunction;